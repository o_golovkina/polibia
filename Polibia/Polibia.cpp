﻿// Polibia.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <Windows.h>
#include <iomanip>
#include <list>

using namespace std;
/// <summary>
/// Класс хранящий данные о букве
/// </summary>
class Symbol
{
    /// <summary>
    /// Сама буква (закрытый параметр)
    /// </summary>
    char symbol;
    /// <summary>
    /// Её позиция столбца в матрице (закрытый параметр)
    /// </summary>
    int column;
    /// <summary>
    /// Её позиция строки в матрице (закрытый параметр)
    /// </summary>
    int row;
public:

    /// <summary>
    /// Получение закрытого значения буквы
    /// </summary>
    /// <returns>букву</returns>
    char Word() { return symbol; }
    /// <summary>
    /// Получение закрытого значения столбца
    /// </summary>
    /// <returns>столбец</returns>
    int Column() { return column; }
    /// <summary>
    /// Получение закрытого значения строки
    /// </summary>
    /// <returns></returns>
    int Row() { return row; }

    /// <summary>
    /// Конструктор класса с параметрами
    /// </summary>
    /// <param name="newSymbol">Буква</param>
    /// <param name="newRow">В какой строке находится буква</param>
    /// <param name="newColumn">В каком стролбце находится буква</param>
    Symbol(char newSymbol, int newRow, int newColumn) : 
        symbol(newSymbol),
        row(newRow), 
        column(newColumn){}
    
    /// <summary>
    /// Конструктор по умолчанию
    /// </summary>
    Symbol();
    ~Symbol();

private:

};

Symbol::Symbol()
{
}

Symbol::~Symbol()
{
}

int main()
{
    //Настройка для вывода и чтения кириллицы
    setlocale(LC_ALL, "Russian");
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    //Переменная для хранения размерности матрицы
    int n = 0;

    //Ввод размерности матрицы
    cout << "Введите размер квадратной матрицы: "; cin >> n;
    
    //Создание двумерного массива из символов
    char** symbols = new char*[n]; //Выделение памяти для n строк

    for (int i = 0; i < n; i++)
        symbols[i] = new char[n]; //Выделение памяти для n столбцов

    cout << "Введите маршрут вписывания букв алфавита в матрицу:" << endl;
        
    //Заполнение двумерного массива данными
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
        {
            cout << "[" << i << "]" << "[" << j << "]: ";
            cin >> symbols[i][j];
        }


    cout << "Маршрут вписывания букв алфавита в матрицу:" << endl << " ";

    //Это вывод для описания номеров столбцов
    for (int i = 0; i < n; i++)
        cout << setw(2) << i;
    cout << endl;

    //Вывод массива с номерами строк
    for (int i = 0; i < n; i++) {
        cout << i;
        for (int j = 0; j < n; j++)
            cout << setw(2) << symbols[i][j];
        cout << endl;
    }

    cout << "Введите слово, которое надо зашифровать: "; string message = "";
    cin >> message;

    list<Symbol*> charList;

    //Заполнение данных для шифрования
    for (int k = 0; k < message.length(); k++)
        for (int i = 0; i < n; i++) 
            for (int j = 0; j < n; j++)
            {
                if (message[k] == symbols[i][j]) {
                    charList.push_back(new Symbol(symbols[i][j], i, j)); //запоминает какая буква, в какой позиции стоит в таблице
                    break;
                }
            }
        
    //Вывод исходного сообщения
    for (const auto& item : charList)
    {
        cout << setw(2) << item->Word();
    }
    cout << endl;

    //Вывод зашифрованного сообщения
    for (const auto& item : charList)
    {
        cout << setw(2) << item->Row() << item->Column();
    }

    cout << "\nРасшифровка: " << endl;

    //Расшифровка
    for (const auto& item : charList)
    {
        cout << setw(2) << symbols[item->Row()][item->Column()];
    }
}
